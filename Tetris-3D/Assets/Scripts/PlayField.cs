﻿using UnityEngine;
using UnityEngine.SceneManagement;



public class PlayField : MonoBehaviour
{
    public static PlayField _instance;
    public int _gridSizeX, _gridSizeY, _gridSizeZ;

    [Header("Blocks")]
    public GameObject[] _blocks;

    [Header("Player Visuals")]
    public GameObject _bottomPanel;
    public GameObject _north, _south, _east, _west;

    public bool[,,] _grid;

    private void Awake()
    {
        _instance = this;
    }

    private void Start()
    {

        _grid = new bool[_gridSizeX, _gridSizeY, _gridSizeZ];
        SpawnNewBlock();
    }

    public Vector3 Round(Vector3 vec)
    {
        return new Vector3(Mathf.RoundToInt(vec.x)
            , Mathf.RoundToInt(vec.y),
            Mathf.RoundToInt(vec.z));
    }

    public bool CheckInsideGrid(Vector3 pos)
    {
        return (Mathf.RoundToInt(pos.x) >= 0 && Mathf.RoundToInt(pos.x) < _gridSizeX &&
                 Mathf.RoundToInt(pos.z) >= 0 && Mathf.RoundToInt(pos.z) < _gridSizeZ &&
                 Mathf.RoundToInt(pos.y) >= 0);


    }

    public void RemoveBlockFromGrid(Transform block)
    {
        foreach(Transform cube in block)
        {
            _grid[Mathf.RoundToInt(cube.position.x), Mathf.RoundToInt(cube.position.y), Mathf.RoundToInt(cube.position.z)] = false;
        }

    }

    public void AddBlockToGrid(Transform block)
    {
        foreach (Transform cube in block)
        {
            _grid[Mathf.RoundToInt(cube.position.x), Mathf.RoundToInt(cube.position.y), Mathf.RoundToInt(cube.position.z)] = true;
        }
    }

    public bool CheckMovement(Vector3 pos)
    {
        return _grid[Mathf.RoundToInt(pos.x), Mathf.RoundToInt(pos.y), Mathf.RoundToInt(pos.z)];

    }

    public void DeleteLayer(Transform block)
    {

        foreach (Transform cube in block)
        {
            if (CheckFullLayer((int)cube.position.y))
            {
                DeleteLayarAt(Mathf.RoundToInt(cube.position.y));
                MoveAllLayersDown(Mathf.RoundToInt(cube.position.y + 1));
            }
        }

    }

    bool CheckFullLayer(int y)
    {
        for (int x = 0; x < _gridSizeX; x++)
        {
            for (int z = 0; z < _gridSizeZ; z++)
            {
                if (_grid[x, y, z] == false)
                {
                    return false;
                }
            }
        }
        return true;
    }

    void DeleteLayarAt(int y)
    {
        GameObject[] cubes = GameObject.FindGameObjectsWithTag("Cube");
       foreach(GameObject cube in cubes)
        {
            if(cube.transform.position.y == y)
            {
                _grid[Mathf.RoundToInt(cube.transform.position.x), Mathf.RoundToInt(cube.transform.position.y), Mathf.RoundToInt(cube.transform.position.z)] = false;
                Destroy(cube.gameObject);
            }
        }
               
            

    }

    void MoveAllLayersDown(int y)
    {
        for (int i = y; i < _gridSizeY; i++)
        {
            MoveOneLayerDown(i);
        }
    }

    void MoveOneLayerDown(int y)
    {
        GameObject[] cubes = GameObject.FindGameObjectsWithTag("Cube");
        foreach (GameObject cube in cubes)
        {
            if (cube.transform.position.y == y)
            {
                _grid[Mathf.RoundToInt(cube.transform.position.x), Mathf.RoundToInt(cube.transform.position.y), Mathf.RoundToInt(cube.transform.position.z)] = false;
                _grid[Mathf.RoundToInt(cube.transform.position.x), Mathf.RoundToInt(cube.transform.position.y-1), Mathf.RoundToInt(cube.transform.position.z)] = true;
                cube.transform.position += Vector3.down;
            }
        }
    }

    public void SpawnNewBlock()
    {
        Vector3 spawnPoint = new Vector3(Mathf.RoundToInt(transform.position.x + (float)_gridSizeX / 2), Mathf.RoundToInt(transform.position.y + _gridSizeY-2), Mathf.RoundToInt(transform.position.z + (float)_gridSizeZ / 2));
        int rnd = Random.Range(0, _blocks.Length);
        GameObject newBlock = Instantiate(_blocks[rnd], spawnPoint, Quaternion.identity) as GameObject;

    }


    public void CheckGameOver(Transform block)
    {
        foreach(Transform cube in block)
        {
            if(cube.position.y >= _gridSizeY-2)
            {
                SceneManager.LoadScene("GameOver");
            }
        }
    }

    private void OnDrawGizmos()
    {

        if (_bottomPanel != null)
        {
            Vector3 scaler = new Vector3((float)_gridSizeX / 10, 1,(float) _gridSizeZ / 10);
            _bottomPanel.transform.localScale = scaler;
            _bottomPanel.transform.position = new Vector3(transform.position.x + (float)_gridSizeX / 2,
                                                             transform.position.y,
                                                             transform.position.z + (float)_gridSizeZ / 2);
            _bottomPanel.GetComponent<MeshRenderer>().sharedMaterial.mainTextureScale = new Vector2(_gridSizeX, _gridSizeZ);
        }
        if (_north != null)
        {
            Vector3 scaler = new Vector3((float)_gridSizeX / 10, 1, (float)_gridSizeY / 10);
            _north.transform.localScale = scaler;
            _north.transform.position = new Vector3(transform.position.x + (float)_gridSizeX / 2,
                                                             transform.position.y + (float)_gridSizeY/2,
                                                             transform.position.z + _gridSizeZ);
            _north.GetComponent<MeshRenderer>().sharedMaterial.mainTextureScale = new Vector2(_gridSizeX, _gridSizeY);
        }
        if (_south != null)
        {
            Vector3 scaler = new Vector3((float)_gridSizeX / 10.0f, 1, (float)_gridSizeY / 10);
            _south.transform.localScale = scaler;
            _south.transform.position = new Vector3(transform.position.x + (float)_gridSizeX / 2,
                                                             transform.position.y + (float)_gridSizeY / 2,
                                                             transform.position.z);

        }
        if (_east != null)
        {
            Vector3 scaler = new Vector3((float)_gridSizeZ / 10.0f, 1, (float)_gridSizeY / 10);
            _east.transform.localScale = scaler;
            _east.transform.position = new Vector3(transform.position.x + _gridSizeX,
                                                             transform.position.y + (float)_gridSizeY / 2,
                                                             transform.position.z + (float)_gridSizeZ/2);
            _east.GetComponent<MeshRenderer>().sharedMaterial.mainTextureScale = new Vector2(_gridSizeZ, _gridSizeY);
        }
        if (_west != null)
        {
            Vector3 scaler = new Vector3((float)_gridSizeZ / 10.0f, 1, (float)_gridSizeY / 10);
            _west.transform.localScale = scaler;
            _west.transform.position = new Vector3(transform.position.x ,
                                                             transform.position.y + (float)_gridSizeY / 2,
                                                             transform.position.z + (float)_gridSizeZ / 2);
        }
    }
}

