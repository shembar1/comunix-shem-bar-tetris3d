﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneControl: MonoBehaviour
{
   
    public void SwitchScene(string nameOfTheScene)
    {
        SceneManager.LoadScene(nameOfTheScene);
    }

}
