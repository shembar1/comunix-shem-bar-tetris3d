﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraControl : MonoBehaviour
{

    Transform _target;
    Transform _rotationTarget;
    Vector3 _lastPos;

    float _sensitivity = 0.25f;
    private void Awake()
    {
        _rotationTarget = transform.parent;
        _target = _rotationTarget.transform.parent;
    }

    void Update()
    {
        transform.LookAt(_target);
        if (Input.GetMouseButtonDown(0))
        {
            _lastPos = Input.mousePosition;
        }
        Orbit();
    }

    void Orbit()
    {
        if (Input.GetMouseButton(0))
        {
  
            Vector3 delta = Input.mousePosition - _lastPos;
            float angleY = -delta.y * _sensitivity;
            float angleX = delta.x * _sensitivity;

            // x axis
            Vector3 angles = _rotationTarget.transform.eulerAngles;
            angles.x += angleY;
            angles.x = ClampAngle(angles.x, -85, 85);
            //angles.x = Mathf.Clamp(angles.x, -89, 89);
            _rotationTarget.transform.eulerAngles = angles;


            // y axis
            _target.RotateAround(_target.position, Vector3.up, angleX);
            _lastPos = Input.mousePosition;

        }
    }

    float ClampAngle(float angle,float from, float to) 
    {
        if (angle < 0)
            angle = 360 + angle;
        if (angle > 180)
            return Mathf.Max(angle, 360 + from);

        return Mathf.Min(angle, to);
    }
}
